*version: 1.5.1*

<img src="img/sw-banner.png" align=center height=300px>

# Description
... *wip*

<!-- [Video/gif of demostration] -->

<br>
<br>

---

## Table of content
+ [Features](#Features)
+ [Gallery](#Gallery)
+ [Dependences](#Dependences)
	- [Requiered](#Requiered-dependences)
	- [Optional](#Optional-dependences)
+ [Install](#Installation)
+ [File structure](#File-structure)
+ [To Do](#To-Do)
+ [Issues](#Issues)

<br>

## Dependences
### Requiered dependences
* **Sway** obviously
* **Azote**        -> Set the wallpaper
* **Mako**         -> Notificacionts
* **Waybar**       -> Status bar
* **Wofi**         -> Launcher
* **SwayLock**     -> Lock the screen

### Optional dependences
* **Grim** for screenshots
* **MPD** - **MPC** for media player CLI
* **Playerctl** for multiple media control, include spotify
* **wl-clipboard** for multiple clipboard operations
* **Brightnessctl** for brightness control
* **TLP** for power managment
* **Gammastep** for control color temperature of your screen

<br>

## Installation
...
