#!/usr/bin/env bash
# you can use ~/.xprofile to autostart on login

# If it´s not running, Run it
run() { [ ! $(pgrep $1) ] && $@& }

# If you want to use XFCE config tools
#run xfce-mcs-manager

#-- POLKIT | PAM | KEYRING
run /usr/libexec/xfce-polkit
#run gnome-keyring-daemon --start

#-- LOCKSCREEN | IDLE
swayidle -w \
	timeout 300 'swaylock -f -c 000000' \
	timeout 600 'swaymsg "output * dpms off"' \
	resume 'swaymsg "output * dpms on"' \
	before-sleep 'swaylock -f -c 000000'

#-- NOTIFICATION
#run mako
run mako --config "${HOME}/.config/mako/config"

#-- STATUS BAR
# waybar_theme="put here code for select the theme name"
LANG=C run waybar -c "${HOME}/.config/waybar/Simpleone/config.json"

#-- WALLPAPER
run ~/.azotebg							# Automatic
#run oguri -c ~/.config/oguri/config				# Dynamic
#swaybg -m fill -o * -i "${HOME}/.config/sway/img/wall.png"	# Default

#-- Demons
run gammastep -c ~/.config/gammastep/config.ini
#run urxvtd
#run thunar --daemon
#run emacs --daemon

#-- Scripts
#run dual_monitor
#run wal -R
