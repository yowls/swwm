#-- SWAY
################
# Reload the configuration file
bindsym $mod+Shift+r reload

# Exit sway (logs you out of your Wayland session)
bindsym $mod+Shift+q exec swaynag -t warning -m 'Do you really want to exit ?' -b 'Yes, exit sway' 'swaymsg exit'


#-- FOCUS
################
# Move your focus around
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# Move focus to the parent container
bindsym $mod+a focus parent

# Kill focused window
bindsym $mod+q kill

# Make the current focus fullscreen
bindsym $mod+f fullscreen

# Toggle the current focus between tiling and floating mode
bindsym $mod+s floating toggle

# Swap focus between the tiling area and the floating area
bindsym $mod+space focus mode_toggle


#-- MOVE WINDOWS
#################
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right


#-- RESIZING WINDOWS
##################
mode "resize" {
    bindsym h resize shrink width 10px
    bindsym j resize grow height 10px
    bindsym k resize shrink height 10px
    bindsym l resize grow width 10px

    # Return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"

# Resize with right mouse button + $mod.
floating_modifier $mod normal


#-- LAYOUTS
###################
# You can "split" the current object of your focus
bindsym $mod+i splith
bindsym $mod+u splitv

# Switch the current container between different layout styles
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+y layout toggle split


#-- SCRATCHPAD
###################
# Bag of holding for windows
# You can send windows there and get them back later.

# Move the currently focused window to the scratchpad
bindsym $mod+Shift+minus move scratchpad

# Show the next scratchpad window or hide the focused scratchpad window.
bindsym $mod+minus scratchpad show
